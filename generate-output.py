import os, sys
from os import listdir
from os.path import isfile, join
import random

done = False

while not done:
    
    print( "" )
    print( "-------------------------" )
    print( "GENERATE STORY" )
    print( "" )

    samplepath = "cv/"
    outpath = "/home/raye/PROJECTS/Machine\ Learning/ML-Fairy-Tale/outputs/"

    onlyfiles = [f for f in listdir( samplepath ) if isfile( join( samplepath, f ) ) ]

    for i in range( len( onlyfiles ) ):
        print( str( i ) + ". \t" + onlyfiles[i] )

    print( "" )
    index = input( "Select a sample: " )

    outputfile = onlyfiles[index].replace( "lm_lstm_", "" ) + "_output"
    
    print( "Output path: " + outpath )
    
    for i in range( 3 ):
        seed = random.randint( 0, 2000 )
        outputfile_withseed = outputfile + "_seed_" + str( seed ) + ".txt"
        command = "th sample.lua " + samplepath + onlyfiles[index] + " -length 1000 -seed " + str( seed )
        command += " > " + outpath + outputfile_withseed
        print( "\t * Creating " + outputfile_withseed + "..." )
        os.system( command )


